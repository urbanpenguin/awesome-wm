syntax enable
set number relativenumber
set wrap linebreak nolist
set cursorline
hi CursorLine cterm=NONE ctermbg=grey ctermfg=white guibg=grey guifg=white
set t_Co=256
set mouse=nicr
set laststatus=2
